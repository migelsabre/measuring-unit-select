<?php
/**
 * @author Mihail Sablin <mihail@webprogrammist.com>
 * @copyright Copyright (c) 2013, Mihail Sablin
 * @licence http://www.opensource.org/licenses/mit-license.php MIT licence
 */
namespace MeasuringUnitSelect;

\Yii::setPathOfAlias('MeasuringUnitSelect', dirname(__FILE__));
/**
 * Class MeasureUnitSelectBehavior
 *
 */
class MeasuringUnitSelectModelBehavior extends \CModelBehavior {
	use MeasuringUnitSelectBehavior;

	/**
	 * @param \CEvent $event
	 */
	public function afterConstruct($event) {
		$this->init();
	}

	/**
	 * @param \CEvent $event
	 * @return boolean
	 */
	public function beforeValidate($event) {
		$this->process();
		return true;
	}

	/**
	 * @param \CEvent $event
	 */
	public function afterValidate($event) {
		$this->initAttributes();
	}
}
