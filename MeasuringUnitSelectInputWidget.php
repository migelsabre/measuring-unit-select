<?php
/**
 * @author Mihail Sablin <mihail@webprogrammist.com>
 * @copyright Copyright (c) 2013, Mihail Sablin
 * @licence http://www.opensource.org/licenses/mit-license.php MIT licence
 */
namespace MeasuringUnitSelect;

\Yii::setPathOfAlias('MeasuringUnitSelect', dirname(__FILE__));
/**
 * Class MeasuringUnitSelectInputWidget
 */
class MeasuringUnitSelectInputWidget extends \CInputWidget {
	public $varSuffix = '_unit';
	/**
	 * @var string Base (default) unit with factor = 1
	 */
	public $baseUnit;
	/**
	 * @var array List of unit labels as array of unit (string) => label (string)
	 */
	public $unitLabels;
	/**
	 * @var array html options for dropdown list, not for text field. For text field specify options via $htmlOptions
	 */
	public $options;

	/**
	 * @return void
	 */
	public function run() {
		list(, $id) = self::resolveNameID($this->model, $this->attribute);
		$this->raiseEvent('onBeforeRender', new \CEvent($this, ['id' => $id]));
		echo \CHtml::activeTextField($this->model, $this->attribute, $this->htmlOptions);
		echo \CHtml::activeDropDownList($this->model, $this->attribute . $this->varSuffix, $this->unitLabels, $this->options);
		$this->raiseEvent('onAfterRender', new \CEvent($this, ['id' => $id]));
	}

	public function onBeforeRender($event) {
	}

	public function onAfterRender($event) {
	}
}
