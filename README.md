Measuring Unit Select
=====================

Information
-----------
* Author: Mihail Sablin <mihail@webprogrammist.com>
* Licence: http://www.opensource.org/licenses/mit-license.php MIT licence
* Project page: https://bitbucket.org/migelsabre/measuring-unit-select/
* Git repository: https://migelsabre@bitbucket.org/migelsabre/measuring-unit-select.git


Description
-----------
Simple extension for [Yii framework](http://www.yiiframework.com/), providing server-side conversion of specified form fields values, given in different measuring units.
Widget looks like text field followed by dropdown menu with list of measuring units. Conversion itself will be done using CModel behavior after submitiing form.

Requirements
------------
* **PHP 5.4.x+**
* Yii framework 1.1.x (Tested under 1.1.13)

Installation
------------
Put files into your directory under application extensions directory (For example into `protected/extension/MeasuringUnitSelect` folder)

Usage
-----

Add behavior configuration to you model `behaviors` method:

		class MyRecord extends \CActiveRecord {
			...
			public function behaviors() {
				return [
					...
					'WalletUnits' => [
						'class' => '\\MeasuringUnitSelect\\MeasuringUnitSelectActiveRecordBehavior',
						'attributes' => ['price'],
						'baseUnit' => 'rub',
						'unitFactors' => ['rub' => 1, 'usd' => function($unit, $attribute) {return 31.5;}],
						'unitLabels' => [
							'rub' => \Yii::app()->locale->getCurrencySymbol('RUB'),
							'usd' => \Yii::app()->locale->getCurrencySymbol('USD'),
						],
					],
					...
				];
			}
			...
		}

If you using `CForm`, add following configuration to your form config:

		return [
			...
			'price' => [
				'type' => '\\MeasuringUnitSelect\\MeasuringUnitSelectInputWidget',
				'unitLabels' => [
					'rub' => \Yii::app()->locale->getCurrencySymbol('RUB'),
					'usd' => \Yii::app()->locale->getCurrencySymbol('USD'),
				],
				'baseUnit' => 'rub',
			],
			...
		];

If you're don't using form builder, add to your view file code that draws input widget, it may be something like that (I dont't tested that code at all):

		$this->beginForm();
		...
		echo $this->widget(
			'\\MeasuringUnitSelect\\MeasuringUnitSelectInputWidget',
			[
				'model' => $model,
				'attribute' => 'price',
				'unitLabels' => [
		            'rub' => \Yii::app()->locale->getCurrencySymbol('RUB'),
		            'usd' => \Yii::app()->locale->getCurrencySymbol('USD'),
		        ],
		        'baseUnit' => 'rub',
			]
		);
		...
		$this->endForm();

