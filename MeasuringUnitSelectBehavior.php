<?php
/**
 * @author Mihail Sablin <mihail@webprogrammist.com>
 * @copyright Copyright (c) 2013, Mihail Sablin
 * @licence http://www.opensource.org/licenses/mit-license.php MIT licence
 */
namespace MeasuringUnitSelect;

trait MeasuringUnitSelectBehavior {
	public $varSuffix = '_unit';
	/**
	 * @var string Base (default) unit with factor = 1
	 */
	public $baseUnit;
	/**
	 * @var bool
	 */
	public $processBaseUnit = false;
	/**
	 * @var int Rounding precision
	 */
	public $precision = 2;
	/**
	 * @var array|callable List of units (or callable returning list) as array of unit (string) => factor (float|callable)
	 */
	public $unitFactors;
	/**
	 * @var array List of unit labels as array of unit (string) => label (string)
	 */
	public $unitLabels;
	/**
	 * @var array|string model attributes to process with behavior as array or comma separated string
	 */
	public $attributes;
	/**
	 * @var null|array|string Include model scenarios as array or comma separated string
	 */
	public $on = null;
	/**
	 * @var null|array|string Exclude model scenarios as array or comma separated string
	 */
	public $except = null;
	/**
	 * @var array
	 */
	private $_attrs = [];
	/**
	 * @var bool
	 */
	private $_initialized = false;

	/**
	 * @return bool
	 */
	public function init() {
		if($this->_initialized) {
			return false;
		}
		if(is_string($this->attributes)) {
			$this->attributes = preg_split('/[\s,]+/', $this->attributes, -1, PREG_SPLIT_NO_EMPTY);
		}
		$this->initAttributes();
		$validator = new \CSafeValidator();
		$validator->attributes = array_keys($this->_attrs);
		$this->getOwner()->validatorList->add($validator);
		$this->_initialized = true;
		return true;
	}

	private function initAttributes() {
		foreach($this->attributes as $field) {
			$this->_attrs[$field . $this->varSuffix] = null;
		}
	}

	public function process() {
		$this->init();
		$owner = $this->owner;
		if($owner->scenario) {
			if(!empty($this->except) && in_array($owner->scenario, is_array($this->except) ? $this->except : preg_split('/[\s,]+/', $this->except, -1, PREG_SPLIT_NO_EMPTY))) {
				return true;
			}
			if(!empty($this->on) && !in_array($owner->scenario, is_array($this->on) ? $this->on : preg_split('/[\s,]+/', $this->on, -1, PREG_SPLIT_NO_EMPTY))) {
				return true;
			}
		}
		foreach($this->attributes as $attribute) {
			$unit = $owner->{$attribute . $this->varSuffix};
			if(!empty($unit)) {
				if($unit != $this->baseUnit || $this->processBaseUnit) {
					$owner->$attribute = $this->calculate($unit, $owner->$attribute);
				}
			}
		}
		return true;
	}

	/**
	 * @param string $unit
	 * @param mixed $attribute
	 * @return float|mixed
	 */
	public function calculate($unit, $attribute) {
		$factor = $this->getFactor($unit);
		if(is_callable($factor)) {
			return $factor($unit, $attribute);
		}
		return round($factor * $attribute, $this->precision);
	}

	/**
	 * @param string $unit
	 * @return int|callable
	 */
	public function getFactor($unit) {
		$units = $this->getUnits();
		return $units[$unit];
	}

	/**
	 * @return array
	 */
	public function getUnits() {
		if(is_callable($this->unitFactors)) {
			return call_user_func($this->unitFactors);
		}
		return $this->unitFactors;
	}

	/**
	 * @param string $name
	 * @return bool
	 */
	public function canGetProperty($name) {
		return array_key_exists($name, $this->_attrs) || isset($this->$name);
	}

	/**
	 * @param string $name
	 * @return bool
	 */
	public function canSetProperty($name) {
		return array_key_exists($name, $this->_attrs) || isset($this->$name);
	}

	/**
	 * @param string $name
	 * @return mixed|null
	 */
	public function __get($name) {
		if(array_key_exists($name, $this->_attrs)) {
			return $this->_attrs[$name];
		}
		return parent::__get($name);
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @return mixed|void
	 */
	public function __set($name, $value) {
		if(array_key_exists($name, $this->_attrs)) {
			$this->_attrs[$name] = $value;
			return;
		}
		parent::__set($name, $value);
	}
}
